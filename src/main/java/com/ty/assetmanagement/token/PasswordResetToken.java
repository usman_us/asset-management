package com.ty.assetmanagement.token;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.ty.assetmanagement.dto.Artisan;

@Entity
public class PasswordResetToken {
	
	private static int expire_date = 10;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String token;
	private Date expireDate;
	@OneToOne
	@JoinColumn
	private Artisan artisan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	

	public static int getExpire_date() {
		return expire_date;
	}

	public static void setExpire_date(int expire_date) {
		PasswordResetToken.expire_date = expire_date;
	}

	public Artisan getArtisan() {
		return artisan;
	}

	public void setArtisan(Artisan artisan) {
		this.artisan = artisan;
	}

	public PasswordResetToken(String token, Artisan arisan) {
		super();
		this.token = token;
		this.artisan = arisan;
		this.expireDate = calculateExpireDdate(expire_date);

	}

	private Date calculateExpireDdate(int expire_date2) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(expire_date2);
		calendar.add(calendar.MINUTE, expire_date2);
		return new Date(calendar.getTime().getTime());
	}

	public PasswordResetToken(String token) {
		super();
		this.token = token;
		this.expireDate = calculateExpireDdate(expire_date);
	}

	public PasswordResetToken() {
		super();
		this.expireDate = calculateExpireDdate(expire_date);
	}
	

}
