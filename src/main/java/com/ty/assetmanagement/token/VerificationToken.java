package com.ty.assetmanagement.token;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.ty.assetmanagement.dto.Artisan;

@Entity
public class VerificationToken {
	private static int expire_date = 10;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String token;
	private Date expireDate;
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn
	private Artisan artisan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Artisan getUser() {
		return artisan;
	}

	public void setUser(Artisan artisan) {
		this.artisan = artisan;
	}

	public VerificationToken(String token, Artisan artisan) {
		super();
		this.token = token;
		this.artisan = artisan;
		this.expireDate = calculateExpireDdate(expire_date);

	}

	private Date calculateExpireDdate(int expire_date2) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(expire_date2);
		calendar.add(calendar.MINUTE, expire_date2);
		return new Date(calendar.getTime().getTime());
	}

	public VerificationToken(String token) {
		super();
		this.token = token;
		this.expireDate = calculateExpireDdate(expire_date);
	}

	public VerificationToken() {
		super();
		this.expireDate = calculateExpireDdate(expire_date);
	}
	
	

}
