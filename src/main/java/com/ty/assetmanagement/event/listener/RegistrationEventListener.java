package com.ty.assetmanagement.event.listener;

import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.email.EmailSenderService;
import com.ty.assetmanagement.event.ResgistrationCompleteEvent;
import com.ty.assetmanagement.service.ArtisanService;



@Component
public class RegistrationEventListener implements ApplicationListener<ResgistrationCompleteEvent> {
	
	
	@Autowired
	ArtisanService artisanService;
	
	@Autowired
	EmailSenderService emailSenderService;

	@Override
	public void onApplicationEvent(ResgistrationCompleteEvent event) {
		
		Artisan artisan = event.getUser();
		String token = UUID.randomUUID().toString();
		artisanService.saveArtisanToken(token,artisan);
		
		String url = event.getApplicationUrl()+"/verifyregistration?token="+token;
		emailSenderService.sendEmail(artisan.getEmail(), url, "Activate your account");
	}

}