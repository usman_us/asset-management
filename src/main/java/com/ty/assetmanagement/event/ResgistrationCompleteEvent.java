package com.ty.assetmanagement.event;

import java.time.Clock;

import org.springframework.context.ApplicationEvent;

import com.ty.assetmanagement.dto.Artisan;


public class ResgistrationCompleteEvent extends ApplicationEvent {
	private  Artisan artisan;
	private  String applicationUrl;
	

	public ResgistrationCompleteEvent(Artisan artisan, String applicationUrl) {
		super(artisan);
		this.artisan =artisan;
		this.applicationUrl = applicationUrl;
	}

	public Artisan getUser() {
		return artisan;
	}

	public void setUser(Artisan artisan) {
		this.artisan = artisan;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public ResgistrationCompleteEvent(Object source, Clock clock) {
		super(source, clock);
		// TODO Auto-generated constructor stub
	}

	public ResgistrationCompleteEvent(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}
	
	
	
	

}
