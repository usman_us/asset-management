package com.ty.assetmanagement.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.assetmanagement.dao.Asset_AllocationDao;
import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.repository.Asset_AllocationRepository;

@Repository
public class Asset_AllocationDaoImpl implements Asset_AllocationDao{
	@Autowired
	private Asset_AllocationRepository allocationRepository;

	public Asset_Allocation saveAsset_Allocation(Asset_Allocation asset_Allocation) {
		Asset_Allocation asset_Allocation2 = (Asset_Allocation) allocationRepository.save(asset_Allocation);
		return asset_Allocation2;
	}

	public Asset_Allocation getAsset_AllocationById(int id) {
		Optional<Asset_Allocation> optional = allocationRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}
	

	public List<Asset_Allocation> getAllAsset_Allocation() {
		return allocationRepository.findAll();
	}

	public Asset_Allocation updateAsset_Allocation(Asset_Allocation asset_Allocation) {
		Asset_Allocation existingAsset_Allocation = allocationRepository.save(asset_Allocation);
		return existingAsset_Allocation;
	}

	public boolean deleteAsset_Allocation(Asset_Allocation asset_Allocation) {
		if (asset_Allocation != null) {
			allocationRepository.delete(asset_Allocation);
			return true;
		}
		return false;
	}

	@Override
	public List<Asset_Allocation> searchAsset_Allocation(String key) {
	
		return allocationRepository.searchAsset_Allocation(key);
	}

	
}
