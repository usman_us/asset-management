package com.ty.assetmanagement.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.assetmanagement.dao.AssetDao;
import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.repository.AssetRepository;

@Repository
public class AssetDaoImpl implements AssetDao{
	@Autowired
	private AssetRepository assetRepository;

	public Asset saveAsset(Asset asset) {
		Asset asset2 = (Asset) assetRepository.save(asset);
		return asset2;
	}

	public Asset getAssetById(int id) {
		Optional<Asset> optional = assetRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public List<Asset> getAllAsset() {
		return assetRepository.findAll();
	}

	public Asset updateAsset(Asset asset) {
		Asset existingAsset = assetRepository.save(asset);
		return existingAsset;
	}

	public boolean deleteAsset(Asset asset) {
		if (asset != null) {
			assetRepository.delete(asset);
			return true;
		}
		return false;
	}

	@Override
	public List<Asset> searchAsset(String key) {
		
		return assetRepository.searchAsset(key);
	}
}
