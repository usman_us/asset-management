package com.ty.assetmanagement.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.assetmanagement.dao.ArtisanDao;
import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.repository.ArtisanRepository;
import com.ty.assetmanagement.repository.TokenRepository;
import com.ty.assetmanagement.token.VerificationToken;

@Repository
public class ArtisanDaoImpl implements ArtisanDao {
	@Autowired
	private ArtisanRepository artisanRepository;
	
	@Autowired
	private TokenRepository tokenRepository;

	public Artisan saveArtisan(Artisan artisan) {
		Artisan artisan2 = (Artisan) artisanRepository.save(artisan);
		return artisan2;
	}

	public Artisan getArtisanById(String id) {
		Optional<Artisan> optional = artisanRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public List<Artisan> getAllArtisans() {
		return artisanRepository.findAll();
	}

	public Artisan updateArtisan(Artisan artisan) {
		Artisan existingArtisan = artisanRepository.save(artisan);
		return existingArtisan;
	}

	@Override
	public boolean deleteArtisan(Artisan artisan) {
		
		if (artisan != null) {
		
			VerificationToken verificationToken = tokenRepository.findByArtisan(artisan);
			
			if(verificationToken!=null) {
				tokenRepository.delete(verificationToken);
			}
			artisanRepository.delete(artisan);
			return true;
		}
		return false;
	}

	@Override
	public List<Artisan> searchArtisan(String key) {
		return artisanRepository.searchDetail(key);

	}
	
}
