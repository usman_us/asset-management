package com.ty.assetmanagement.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.assetmanagement.dao.WingDao;
import com.ty.assetmanagement.dto.Wing;
import com.ty.assetmanagement.repository.WingRepository;

@Repository
public class WingDaoImpl implements WingDao{
	@Autowired
	private WingRepository wingRepository;

	public Wing saveWing(Wing wing) {
		return wingRepository.save(wing);
		
	}

	public Wing getWingById(int id) {
		Optional<Wing> optional = wingRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public List<Wing> getAllWing() {
		return wingRepository.findAll();
	}

	public Wing updateWing(Wing wing) {
		Wing existingWing = wingRepository.save(wing);
		return existingWing;
	}

	public boolean deleteWing(Wing wing) {
		if (wing != null) {
			wingRepository.delete(wing);
			return true;
		}
		return false;
	}

}
