package com.ty.assetmanagement.dao;

import java.util.List;

import com.ty.assetmanagement.dto.Wing;

public interface WingDao {
	public Wing saveWing(Wing wing);

	public Wing getWingById(int id);

	public List<Wing> getAllWing();

	public Wing updateWing(Wing wing);

	public boolean deleteWing(Wing wing);

}
