package com.ty.assetmanagement.dao;

import java.util.List;

import com.ty.assetmanagement.dto.Asset;

public interface AssetDao {
	public Asset saveAsset(Asset asset);

	public Asset getAssetById(int id);

	public List<Asset> getAllAsset();

	public Asset updateAsset(Asset asset);

	public boolean deleteAsset(Asset asset);

	public List<Asset> searchAsset(String key);
}
