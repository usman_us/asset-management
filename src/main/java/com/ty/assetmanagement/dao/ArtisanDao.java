package com.ty.assetmanagement.dao;

import java.util.List;

import com.ty.assetmanagement.dto.Artisan;

public interface ArtisanDao {
	public Artisan saveArtisan(Artisan artisan);

	public Artisan getArtisanById(String id);

	public List<Artisan> getAllArtisans();

	public Artisan updateArtisan(Artisan artisan);

	public boolean deleteArtisan(Artisan artisan);

	public List<Artisan> searchArtisan(String key);

}