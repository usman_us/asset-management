package com.ty.assetmanagement.dao;

import java.util.List;


import com.ty.assetmanagement.dto.Asset_Allocation;

public interface Asset_AllocationDao {
	public Asset_Allocation saveAsset_Allocation(Asset_Allocation asset_Allocation);

	public Asset_Allocation getAsset_AllocationById(int id);

	public List<Asset_Allocation> getAllAsset_Allocation();

	public Asset_Allocation updateAsset_Allocation(Asset_Allocation asset_Allocation);

	public boolean deleteAsset_Allocation(Asset_Allocation asset_Allocation);

	public  List<Asset_Allocation> searchAsset_Allocation(String key);

}
