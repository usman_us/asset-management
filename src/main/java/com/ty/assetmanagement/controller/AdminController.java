package com.ty.assetmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.service.AssetService;
import com.ty.assetmanagement.util.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
public class AdminController {
	@Autowired
	private AssetService assetService;
	@PostMapping("asset")
	@ApiOperation("To add asset to an inventory")
	@ApiResponses({ @ApiResponse(code = 200,message = "Added asset successfully "),
		@ApiResponse(code = 404, message = "Class not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<ResponseStructure<Asset>> saveAsset(@RequestBody Asset asset) {
		return assetService.saveAsset(asset);
	}

	@GetMapping("asset/{id}")
	@ApiOperation("To Update Asset By Id")
	@ApiResponses({ @ApiResponse(code = 200,message = "Asset Retrived"),
		@ApiResponse(code = 404, message = "Class not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<ResponseStructure<Asset>> getAssetById(@PathVariable int id) {
		return assetService.getAssetById(id);
	}

	@GetMapping("asset")
	@ApiOperation("To get All the Assets in the Inventory")
	@ApiResponses({ @ApiResponse(code = 200,message = "To get All the Assets"),
		@ApiResponse(code = 404, message = "Class not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<ResponseStructure<List<Asset>>> getAllAsset() {
		return assetService.getAllAsset();
	}

	@PutMapping("asset/{id}")
	@ApiOperation("To Update the Asset By Id")
	@ApiResponses({ @ApiResponse(code = 200,message = "Update Specific Asset Based on given id"),
		@ApiResponse(code = 404, message = "Class not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<ResponseStructure<Asset>> updateAsset( @RequestBody Asset asset,@PathVariable int id) {
		return assetService.updateAsset(asset,id);
	}

	@DeleteMapping("asset")
	@ApiOperation("To delete the Asset")
	@ApiResponses({ @ApiResponse(code = 200,message = "Asset Deleted"),
		@ApiResponse(code = 404, message = "Class not Found"),
		@ApiResponse(code = 500, message = "Internal Server Error") })
	public ResponseEntity<ResponseStructure<Boolean>> deleteBranch(@RequestParam int id) {
		return assetService.deleteAsset(id);
	}
}



