package com.ty.assetmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.service.Asset_AllocationService;
import com.ty.assetmanagement.util.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ManagerController {
	
	@Autowired
	Asset_AllocationService allocationService;
	
	@PostMapping("/assetallocation/{artisanid}/{assetid}")
	@ApiOperation("To raise the request ")
	@ApiResponses({ @ApiResponse(code = 200, message = "request sent"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Asset_Allocation>> raisingRequest(@PathVariable("assetid") int assetId,@RequestBody Asset_Allocation assetAllocation,@PathVariable("artisanid") String artisanId){
		return allocationService.saveAssetAllocation(assetAllocation, artisanId,assetId);
		
	}

	
	@GetMapping("/assetallocation/{artisanId}")
	@ApiOperation("To get the request by id ")
	@ApiResponses({ @ApiResponse(code = 200, message = "request retrieved"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Asset_Allocation>> getRequestByid(@RequestBody Asset_Allocation assetAllocation,@PathVariable("artisanId") int artisanId){
		return allocationService.getAssetAllocationById(artisanId);
		
	}
	
	@GetMapping("/assetallocation")
	@ApiOperation("to get all the requests ")
	@ApiResponses({ @ApiResponse(code = 200, message = "requests retrieved"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> getAllRequests(){
		return allocationService.getAllAllocations();
		
	}
	@DeleteMapping("/artisanid")
	@ApiOperation("to delete the requests ")
	@ApiResponses({ @ApiResponse(code = 200, message = "requests retrieved"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Boolean>> deleteRequest(@PathVariable("artisanid") int id){
		return allocationService.deleteAllocation(id);
		
	}
	
	
}
