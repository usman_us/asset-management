package com.ty.assetmanagement.controller;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.email.EmailSenderService;
import com.ty.assetmanagement.model.PasswordModel;
import com.ty.assetmanagement.service.ArtisanService;
import com.ty.assetmanagement.service.AssetService;
import com.ty.assetmanagement.service.Asset_AllocationService;
import com.ty.assetmanagement.util.ResponseStructure;
import com.ty.assetmanagement.util.UrlCreation;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ArtisanController {
	
	@Autowired
	ArtisanService artisanService;
	
	@Autowired
	AssetService assetService;
	
	@Autowired
	Asset_AllocationService allocationService;
	
	@Autowired
	UrlCreation urlCreation;
	
	@Autowired
	EmailSenderService emailSenderService;
	
	
	@PostMapping("/artisan/{wing_id}")
	@ApiOperation("To save Artisan data")
	@ApiResponses({ @ApiResponse(code = 200, message = "data saved"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 406, message = "email ID is alredy present"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Artisan>> saveArtisan(@RequestBody Artisan artisan,@PathVariable("wing_id") int wing_id,final HttpServletRequest request){
		ResponseEntity<ResponseStructure<Artisan>> entity =  artisanService.saveArtisan(artisan, wing_id,request);
		return entity;
	}
	
	
	@GetMapping("/verifyregistration")
	@ApiOperation("To complete registration of user when user click activation link")
	@ApiResponses({ @ApiResponse(code = 200, message = "data saved"),
			@ApiResponse(code = 404, message = "wing Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<String>> validateUser(@RequestParam("token") String token) {
		ResponseEntity<ResponseStructure<String>> result = artisanService.isArtisanValid(token);
		if (result.getBody().getData().equalsIgnoreCase("valid")) {
			return result;
		} else
			return result;
	}
	
	@DeleteMapping("/artisan/{artisanId}")
	@ApiOperation("To delete artisan by using ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "data deleted"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Boolean>> deleteArtisan(@PathVariable("artisanId") String artisanId){
		return artisanService.deleteArtisan(artisanId);
	}
	
	@PutMapping("/artisan/{artisanId}")
	@ApiOperation("To update Artisan")
	@ApiResponses({ @ApiResponse(code = 200, message = "data updated"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Artisan>> updateArtisan(@RequestBody Artisan artisan,@PathVariable("artisanId") String artisanId){
		return artisanService.updateArtisan(artisan, artisanId);
	}
	
	
	@GetMapping("/artisan")
	@ApiOperation("To get all artisan")
	@ApiResponses({ @ApiResponse(code = 200, message = "data retrived"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Artisan>>> getAllArtisan(){
		return artisanService.getAllArtisan();
	}
	
	@GetMapping("/artisan/{artisanId}")
	@ApiOperation("To get Artisan  by ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "data retrived"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<Artisan>> getArtisan(@PathVariable("artisanId") String artisanId){
		return artisanService.getArtisanById(artisanId);
	}
	
	
	@GetMapping("/findartisan")
	@ApiOperation("To search the data from Artisan table")
	@ApiResponses({ @ApiResponse(code = 200, message = "data retrived"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Artisan>>> searchArtisan(@RequestParam String key){
		return artisanService.searchArtisan(key);
		
	}
	
	
	@GetMapping("/findasset")
	@ApiOperation("To search the data from Artisan table")
	@ApiResponses({ @ApiResponse(code = 200, message = "data retrived"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Asset>>> searchAsset(@RequestParam String key){
		return assetService.searchAsset(key);	
	}
	
	@GetMapping("/findassetallocation")
	@ApiOperation("To search the data from Artisan table")
	@ApiResponses({ @ApiResponse(code = 200, message = "data retrived"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> searchAsset_Allocation(@RequestParam String key){
		return allocationService.searchAsset_Allocation(key);	
	}
	
	@GetMapping("/resetpassword")
	public String resetPassword(@RequestBody PasswordModel model, HttpServletRequest request) {
		Optional<Artisan> artisan = artisanService.findUserByEmail(model.getEmail());
		String url = "";
		if (artisan != null) {
			String token = UUID.randomUUID().toString();
			artisanService.createPasswordReserTokenForUser(artisan.get(), token);
//			url = passwordReserTokenEmail(artisan, urlCreation.applicationUrl(request), token);
			url = urlCreation.applicationUrl(request) + "/savepassword?token=" + token;
			emailSenderService.sendEmail(artisan.get().getEmail(), url, "Activate your account");
			return "reset password email send";
		}
		return "no user found";
	}
	
	@PostMapping("/savepassword")
	public String savePassword(@RequestParam("token") String token, @RequestBody PasswordModel model) {
		String result = artisanService.validatePasswordResetToken(token);
		if (!result.equalsIgnoreCase("valid")) {
			return result;
		}

		Optional<Artisan> artisan = artisanService.getUserByPasswordResetToken(token);
		if (artisan.isPresent()) {
			artisanService.changePassword(artisan.get(), model.getNewPassword(), token);

			return "password changed";
		} else
			return "invalid token";

	}
}
