package com.ty.assetmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.assetmanagement.token.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Integer> {

	PasswordResetToken findByToken(String token);
	
}
