package com.ty.assetmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ty.assetmanagement.dto.Asset_Allocation;

public interface Asset_AllocationRepository extends JpaRepository<Asset_Allocation, Integer> {
	@Query("select a from Asset_Allocation a where  "
			+"CONCAT(a.id,' ',a.enabled,' ',a.name,' ')"
			+"like %?1%")
	List<Asset_Allocation> searchAsset_Allocation(String key);

}
