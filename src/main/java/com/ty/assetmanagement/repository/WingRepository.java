package com.ty.assetmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.assetmanagement.dto.Wing;

public interface WingRepository extends JpaRepository<Wing, Integer>{

}
