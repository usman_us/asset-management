package com.ty.assetmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ty.assetmanagement.dto.Asset;

public interface AssetRepository extends JpaRepository<Asset, Integer> {
	@Query("select a from Asset a where  "
			+"CONCAT(a.id,' ',a.name,' ',a.status,' ',a.quantity,' ')"
			+"like %?1%")
	List<Asset> searchAsset(String key);

}
