package com.ty.assetmanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ty.assetmanagement.dto.Artisan;

public interface ArtisanRepository extends JpaRepository<Artisan, Integer>{

	@Query("select a from Artisan a where  "
			+"CONCAT(a.id,' ',a.name,' ',a.email,' ',a.role,' ',a.hired_date,' ')"
			+"like %?1%")
	List<Artisan> searchDetail(@Param("key")String key);
	Artisan getArtisanByEmail(String email);
	
	Optional<Artisan> findById( String id);
	
	@Query("select a from Artisan a where month(a.hired_date) = ?1")
	List<Artisan> getByYearAndMonth(int month);

}
