package com.ty.assetmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.token.VerificationToken;

public interface TokenRepository extends JpaRepository<VerificationToken, Integer> {

	VerificationToken findByToken(String token);

	VerificationToken findByArtisan(Artisan artisan);

}
