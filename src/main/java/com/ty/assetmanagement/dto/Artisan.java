package com.ty.assetmanagement.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ty.assetmanagement.util.StringPrefixedSequenceIdGenerator;

@Entity
public class Artisan {
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "arti_seq")
	@GenericGenerator(name = "arti_seq", strategy = "com.ty.assetmanagement.util.StringPrefixedSequenceIdGenerator", parameters = {
			@Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "50"),
			@Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Arti_"),
			@Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%03d") })
			@Id		
	private String id;
	private String name;
	private String email;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;
	private Date hired_date;
	private String role;
	private int mngr_id;
	private boolean enabled;
	private boolean user_Master;
	@OneToMany(mappedBy = "artisan")
	@JsonIgnore
	private List<Asset_Allocation> asset_Allocations;
	@ManyToOne
	@JoinColumn
	private Wing wing;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getHired_date() {
		return hired_date;
	}
	public void setHired_date(Date hired_date) {
		this.hired_date = hired_date;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getMngr_id() {
		return mngr_id;
	}
	public void setMngr_id(int mngr_id) {
		this.mngr_id = mngr_id;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isUser_Master() {
		return user_Master;
	}
	public void setUser_Master(boolean user_Master) {
		this.user_Master = user_Master;
	}
	public List<Asset_Allocation> getAsset_Allocations() {
		return asset_Allocations;
	}
	public void setAsset_Allocations(List<Asset_Allocation> asset_Allocations) {
		this.asset_Allocations = asset_Allocations;
	}
	public Wing getWing() {
		return wing;
	}
	public void setWing(Wing wing) {
		this.wing = wing;
	}
	
	
	
}
