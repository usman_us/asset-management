package com.ty.assetmanagement.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebSecurity
@Configuration
@EnableSwagger2
public class AssetManagmentConfig {

	private static final String[] white_list_url = { "/artisan/{wing_id}", "/json1", "/resetpassword", "/savepassword",
			"/assetallocation/{artisanId}", "/verifyregistration" };

	@Bean
	public PasswordEncoder encodePassword() {
		return new BCryptPasswordEncoder(11);
	}

	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.cors().and().csrf().disable().authorizeHttpRequests().antMatchers(white_list_url).permitAll();
		return httpSecurity.build();
	}

	@Bean
	public Docket getDocket() {
		Contact contact = new Contact("AssetManagementSystem", "https//testyantra.com", "teamicons@mail.com");
		List<VendorExtension> extension = new ArrayList<VendorExtension>();

		ApiInfo apiInfo = new ApiInfo("AssetManagementSystem Api Documentations",
				"API's to include asset in the inventory ", "TYP-AssetManagementSystem-snappShhot 1.0.1", "", contact,
				"Licence 1101", "https://jsonbeautifier.org", extension);

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.ty.assetmanagement")).build().apiInfo(apiInfo)
				.useDefaultResponseMessages(false);
	}

}
