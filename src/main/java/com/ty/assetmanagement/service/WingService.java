package com.ty.assetmanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.assetmanagement.dto.Wing;
import com.ty.assetmanagement.util.ResponseStructure;

public interface WingService {
	
	public ResponseEntity<ResponseStructure<Wing>> saveWing(Wing wing);
	
	public ResponseEntity<ResponseStructure<Wing>> getWingById(int wingId);
	
	public ResponseEntity<ResponseStructure<List<Wing>>> getAllWing();
	
	public ResponseEntity<ResponseStructure<Wing>> updateWing(Wing wing , int wingId);
	
	public ResponseEntity<ResponseStructure<Boolean>> deleteWing(int wingId);

}
