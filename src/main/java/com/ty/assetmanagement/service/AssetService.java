package com.ty.assetmanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.util.ResponseStructure;

public interface AssetService {
	
	public ResponseEntity<ResponseStructure<Asset>> saveAsset(Asset asset);
	
	public ResponseEntity<ResponseStructure<Asset>> getAssetById(int assetId);
	
	public ResponseEntity<ResponseStructure<List<Asset>>> getAllAsset();
	
	public ResponseEntity<ResponseStructure<Asset>> updateAsset(Asset asset,int assetId);
	
	public ResponseEntity<ResponseStructure<Boolean>>  deleteAsset(int assetId);

	public ResponseEntity<ResponseStructure<List<Asset>>> searchAsset(String key);
	

}
