package com.ty.assetmanagement.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.util.ResponseStructure;

public interface ArtisanService {
	public ResponseEntity<ResponseStructure<Artisan>> saveArtisan(Artisan artisan, int wingId,final HttpServletRequest request);

	public ResponseEntity<ResponseStructure<Artisan>> getArtisanById(String artisanId);
 
	public ResponseEntity<ResponseStructure<List<Artisan>>> getAllArtisan();

	public ResponseEntity<ResponseStructure<Artisan>> updateArtisan(Artisan artisan, String artisanId);

	public ResponseEntity<ResponseStructure<Boolean>> deleteArtisan(String artisanId);

	public void saveArtisanToken(String token, Artisan artisan);

	public ResponseEntity<ResponseStructure<String>> isArtisanValid(String token);

	public ResponseEntity<ResponseStructure<List<Artisan>>> searchArtisan(String key);

	public Optional<Artisan> findUserByEmail(String email);
	
	public void createPasswordReserTokenForUser(Artisan artisan, String token) ;

	public String validatePasswordResetToken(String token);

	public Optional<Artisan> getUserByPasswordResetToken(String token);

	public void changePassword(Artisan artisan, String newPassword, String token);

	


}
