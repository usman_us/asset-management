package com.ty.assetmanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.util.ResponseStructure;

public interface Asset_AllocationService {
	
	public ResponseEntity<ResponseStructure<Asset_Allocation>> saveAssetAllocation(Asset_Allocation assetAllocation,String artisanId, int assetId);
	
	public ResponseEntity<ResponseStructure<Asset_Allocation>> getAssetAllocationById(int allocationId);
	
	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> getAllAllocations();
	
	public ResponseEntity<ResponseStructure<Asset_Allocation>> updateAllocation(Asset_Allocation allocation,int assetAllocationId);
	
	public ResponseEntity<ResponseStructure<Boolean>> deleteAllocation(int allocationId);

	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> searchAsset_Allocation(String key);
	
	
	

}
