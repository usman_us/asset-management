package com.ty.assetmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.assetmanagement.dao.WingDao;
import com.ty.assetmanagement.dto.Wing;
import com.ty.assetmanagement.service.WingService;
import com.ty.assetmanagement.util.ResponseStructure;

@Service
public class WingServiceImpl implements WingService {

	@Autowired
	private WingDao daoImpl;

	@Override
	public ResponseEntity<ResponseStructure<Wing>> saveWing(Wing wing) {
		ResponseStructure<Wing> responseStructure = new ResponseStructure<Wing>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(daoImpl.saveWing(wing));
		ResponseEntity<ResponseStructure<Wing>> responseEntity = new ResponseEntity<ResponseStructure<Wing>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Wing>> getWingById(int wingId) {
		Wing wing = daoImpl.getWingById(wingId);
		if (wing != null) {
			ResponseStructure<Wing> responseStructure = new ResponseStructure<Wing>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(daoImpl.getWingById(wingId));
			ResponseEntity<ResponseStructure<Wing>> responseEntity = new ResponseEntity<ResponseStructure<Wing>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		} else {
			return null;
		}
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Wing>>> getAllWing() {
		ResponseStructure<List<Wing>> responseStructure = new ResponseStructure<List<Wing>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(daoImpl.getAllWing());
		ResponseEntity<ResponseStructure<List<Wing>>> responseEntity = new ResponseEntity<ResponseStructure<List<Wing>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Wing>> updateWing(Wing wing, int wingId) {
		ResponseStructure<Wing> responseStructure = new ResponseStructure<Wing>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(daoImpl.updateWing(wing));
		ResponseEntity<ResponseStructure<Wing>> responseEntity = new ResponseEntity<ResponseStructure<Wing>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Boolean>> deleteWing(int wingId) {
		Wing wing = daoImpl.getWingById(wingId);
		ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Deleted successfull");
		responseStructure.setData(daoImpl.deleteWing(wing));
		ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

}
