package com.ty.assetmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.assetmanagement.dao.impl.AssetDaoImpl;
import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.service.AssetService;
import com.ty.assetmanagement.util.ResponseStructure;

@Service
public class AssetServiceImpl implements AssetService {
	@Autowired
	private AssetDaoImpl assetDaoImpl;

	@Override
	public ResponseEntity<ResponseStructure<Asset>> saveAsset(Asset asset) {
		ResponseStructure<Asset> responseStructure = new ResponseStructure<Asset>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(assetDaoImpl.saveAsset(asset));
		ResponseEntity<ResponseStructure<Asset>> responseEntity = new ResponseEntity<ResponseStructure<Asset>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	}

	@Override
	public ResponseEntity<ResponseStructure<List<Asset>>> getAllAsset() {
		ResponseStructure<List<Asset>> responseStructure = new ResponseStructure<List<Asset>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(assetDaoImpl.getAllAsset());
		ResponseEntity<ResponseStructure<List<Asset>>> responseEntity = new ResponseEntity<ResponseStructure<List<Asset>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;

	}

	@Override
	public ResponseEntity<ResponseStructure<Asset>> updateAsset(Asset asset, int assetId) {
		ResponseStructure<Asset> responseStructure = new ResponseStructure<Asset>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successfull");
		responseStructure.setData(assetDaoImpl.updateAsset(asset));
		ResponseEntity<ResponseStructure<Asset>> responseEntity = new ResponseEntity<ResponseStructure<Asset>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Boolean>> deleteAsset(int assetId) {
		Asset asset = assetDaoImpl.getAssetById(assetId);
		ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Deleted successfull");
		responseStructure.setData(assetDaoImpl.deleteAsset(asset));
		ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Asset>> getAssetById(int assetId) {
		Asset asset = assetDaoImpl.getAssetById(assetId);
		if (asset != null) {
			ResponseStructure<Asset> responseStructure = new ResponseStructure<Asset>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(assetDaoImpl.getAssetById(assetId));
			ResponseEntity<ResponseStructure<Asset>> responseEntity = new ResponseEntity<ResponseStructure<Asset>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		} else {
			return null;
		}
		

	}

	@Override
	public ResponseEntity<ResponseStructure<List<Asset>>> searchAsset(String key) {
		
		
		ResponseStructure<List<Asset>>responseStructure = new ResponseStructure<List<Asset>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successful");
		responseStructure.setData(assetDaoImpl.searchAsset(key));
		ResponseEntity<ResponseStructure<List<Asset>>> responseEntity = new ResponseEntity<ResponseStructure<List<Asset>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

}
