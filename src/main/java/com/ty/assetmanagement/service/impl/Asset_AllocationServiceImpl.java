package com.ty.assetmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.assetmanagement.dao.ArtisanDao;
import com.ty.assetmanagement.dao.AssetDao;
import com.ty.assetmanagement.dao.Asset_AllocationDao;
import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.service.Asset_AllocationService;
import com.ty.assetmanagement.util.ResponseStructure;

@Service
public class Asset_AllocationServiceImpl implements Asset_AllocationService {
	@Autowired
	Asset_AllocationDao allocationDao;
	
	@Autowired
	ArtisanDao artisanDao;
	
	@Autowired
	AssetDao assetDao;
	
	
	@Override
	public ResponseEntity<ResponseStructure<Asset_Allocation>> saveAssetAllocation(Asset_Allocation assetAllocation,String artisanId,int assetId) {
		Artisan artisan = artisanDao.getArtisanById(artisanId);
		Asset asset = assetDao.getAssetById(assetId);
		assetAllocation.setArtisan(artisan);
		assetAllocation.setAsset(asset);
		ResponseStructure<Asset_Allocation> responseStructure = new ResponseStructure<Asset_Allocation>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successful");
		responseStructure.setData(allocationDao.saveAsset_Allocation(assetAllocation));
		ResponseEntity<ResponseStructure<Asset_Allocation>> responseEntity = new ResponseEntity<ResponseStructure<Asset_Allocation>>
		(responseStructure,HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Asset_Allocation>> getAssetAllocationById(int allocationId) {
		Asset_Allocation allocation = allocationDao.getAsset_AllocationById(allocationId);
		if(allocation!=null) {
			ResponseStructure<Asset_Allocation> responseStructure = new ResponseStructure<Asset_Allocation>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(allocationDao.getAsset_AllocationById(allocationId));
			ResponseEntity<ResponseStructure<Asset_Allocation>> responseEntity = new ResponseEntity<ResponseStructure<Asset_Allocation>>
			(responseStructure,HttpStatus.OK);
			return responseEntity;
		}else {
			return null;
		}
	}
	@Override
	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> getAllAllocations() {
		ResponseStructure<List<Asset_Allocation>> responseStructure = new ResponseStructure<List<Asset_Allocation>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successful");
		responseStructure.setData(allocationDao.getAllAsset_Allocation());
		ResponseEntity<ResponseStructure<List<Asset_Allocation>>> responseEntity = new ResponseEntity<ResponseStructure<List<Asset_Allocation>>>
		(responseStructure,HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Asset_Allocation>> updateAllocation(Asset_Allocation allocation,int assetAllocationId) {
		Asset_Allocation allocation2= allocationDao.getAsset_AllocationById(assetAllocationId);
		if(allocation2!=null) {
			ResponseStructure<Asset_Allocation> responseStructure = new ResponseStructure<Asset_Allocation>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Successful");
			responseStructure.setData(allocationDao.updateAsset_Allocation(allocation));
			ResponseEntity<ResponseStructure<Asset_Allocation>> responseEntity = new ResponseEntity<ResponseStructure<Asset_Allocation>>
			(responseStructure,HttpStatus.OK);
			return responseEntity;
		}else {
			return null;
		}
		
	}

	@Override
	public ResponseEntity<ResponseStructure<Boolean>> deleteAllocation(int allocationId) {
		Asset_Allocation asset_Allocation = allocationDao.getAsset_AllocationById(allocationId);
		if(asset_Allocation!=null) {
			ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(allocationDao.deleteAsset_Allocation(asset_Allocation));
			ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>
			(responseStructure,HttpStatus.OK);
			return responseEntity;
		}else {
			ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
			responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
			responseStructure.setMessage("not found");
			responseStructure.setData(false);
			ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>
			(responseStructure,HttpStatus.NOT_FOUND);
			return responseEntity;
	}

}
	
	@Override
	public ResponseEntity<ResponseStructure<List<Asset_Allocation>>> searchAsset_Allocation(String key) {
		ResponseStructure<List<Asset_Allocation>> responseStructure = new ResponseStructure<List<Asset_Allocation>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successful");
		responseStructure.setData(allocationDao.searchAsset_Allocation(key));
		ResponseEntity<ResponseStructure<List<Asset_Allocation>>> responseEntity = new ResponseEntity<ResponseStructure<List<Asset_Allocation>>>
		(responseStructure,HttpStatus.OK);
		return responseEntity;
	
	}
}
