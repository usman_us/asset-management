package com.ty.assetmanagement.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ty.assetmanagement.dao.ArtisanDao;
import com.ty.assetmanagement.dao.WingDao;
import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.dto.Wing;
import com.ty.assetmanagement.event.ResgistrationCompleteEvent;
import com.ty.assetmanagement.repository.ArtisanRepository;
import com.ty.assetmanagement.repository.PasswordResetTokenRepository;
import com.ty.assetmanagement.repository.TokenRepository;
import com.ty.assetmanagement.service.ArtisanService;
import com.ty.assetmanagement.token.PasswordResetToken;
import com.ty.assetmanagement.token.VerificationToken;
import com.ty.assetmanagement.util.ResponseStructure;
import com.ty.assetmanagement.util.UrlCreation;

@Service
public class ArtisanServiceImpl implements ArtisanService {

	@Autowired
	private ArtisanDao artisanDao;

	@Autowired
	private WingDao wingDao;

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	ApplicationEventPublisher publisher;

	@Autowired
	UrlCreation urlCreation;

	@Autowired
	private ArtisanRepository artisanRepository;

	@Autowired
	PasswordResetTokenRepository passwordResetTokenRepository;

	@Override
	public ResponseEntity<ResponseStructure<Artisan>> saveArtisan(Artisan artisan, int wingId,
			final HttpServletRequest request) {
		if (artisanRepository.getArtisanByEmail(artisan.getEmail()) == null) {

			Calendar calendar = Calendar.getInstance();
			if (artisan.getHired_date().getDate() >= calendar.getTime().getDate()
					&& artisan.getHired_date().getMonth() >= calendar.getTime().getMonth()
					&& artisan.getHired_date().getYear() >= calendar.getTime().getYear()) {
				publisher.publishEvent(new ResgistrationCompleteEvent(artisan, urlCreation.applicationUrl(request)));
				Wing wing = wingDao.getWingById(wingId);
				artisan.setWing(wing);
				artisan.setEnabled(false);
				artisan.setPassword(encoder.encode(artisan.getPassword()));
				ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
				responseStructure.setStatus(HttpStatus.OK.value());
				responseStructure.setMessage("successfull");
				responseStructure.setData(artisanDao.saveArtisan(artisan));
				ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
						responseStructure, HttpStatus.OK);
				return responseEntity;
			} else {

				ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
				responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
				responseStructure.setMessage("Hire date is not a present date or upcoming date");
				responseStructure.setData(null);
				ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
						responseStructure, HttpStatus.NOT_FOUND);
				return responseEntity;
			}

		} else {
			ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
			responseStructure.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			responseStructure.setMessage("Email id already present");
			responseStructure.setData(null);
			ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
					responseStructure, HttpStatus.NOT_ACCEPTABLE);
			return responseEntity;

		}

	}

	@Override
	public ResponseEntity<ResponseStructure<Artisan>> getArtisanById(String artisanId) {
		Artisan artisan = artisanDao.getArtisanById(artisanId);
		if (artisan != null) {
			ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(artisanDao.getArtisanById(artisanId));
			ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		} else {
			ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
			responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
			responseStructure.setMessage("not found");
			responseStructure.setData(artisanDao.getArtisanById(null));
			ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
					responseStructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}

	}

	@Override
	public ResponseEntity<ResponseStructure<List<Artisan>>> getAllArtisan() {
		ResponseStructure<List<Artisan>> responseStructure = new ResponseStructure<List<Artisan>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("successful");
		responseStructure.setData(artisanDao.getAllArtisans());
		ResponseEntity<ResponseStructure<List<Artisan>>> responseEntity = new ResponseEntity<ResponseStructure<List<Artisan>>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Artisan>> updateArtisan(Artisan artisan, String artisanId) {

		Artisan artisan1 = artisanDao.getArtisanById(artisanId);
		if (artisan1 != null) {
			ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("successful");
			responseStructure.setData(artisanDao.updateArtisan(artisan1));
			ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		} else {
			ResponseStructure<Artisan> responseStructure = new ResponseStructure<Artisan>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("not found");
			responseStructure.setData(artisanDao.updateArtisan(artisan1));
			ResponseEntity<ResponseStructure<Artisan>> responseEntity = new ResponseEntity<ResponseStructure<Artisan>>(
					responseStructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}

	}

	@Override
	public ResponseEntity<ResponseStructure<Boolean>> deleteArtisan(String artisanId) {
		Artisan artisan = artisanDao.getArtisanById(artisanId);
		if (artisan != null) {
			ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("success");
			responseStructure.setData(artisanDao.deleteArtisan(artisan));
			ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		} else {
			ResponseStructure<Boolean> responseStructure = new ResponseStructure<Boolean>();
			responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
			responseStructure.setMessage("not found");

			responseStructure.setData(false);
			ResponseEntity<ResponseStructure<Boolean>> responseEntity = new ResponseEntity<ResponseStructure<Boolean>>(
					responseStructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}

	}

	@Override
	public void saveArtisanToken(String token, Artisan artisan) {
		VerificationToken verificationToken = new VerificationToken();
		verificationToken.setToken(token);
		verificationToken.setUser(artisan);
		tokenRepository.save(verificationToken);
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> isArtisanValid(String token) {
		VerificationToken tokenVerification = tokenRepository.findByToken(token);
		if (tokenVerification == null) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
			responseStructure.setMessage("Token not exist");
			responseStructure.setData("invalid");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}

		Artisan artisan = tokenVerification.getUser();
		Calendar calendar = Calendar.getInstance();
		if ((tokenVerification.getExpireDate().getTime() - calendar.getTime().getTime()) >= 0) {
			ResponseStructure<String> responseStructure = new ResponseStructure<String>();
			responseStructure.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			responseStructure.setMessage("Token time expired");
			responseStructure.setData("expired");
			ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
					responseStructure, HttpStatus.NOT_ACCEPTABLE);
			return responseEntity;
		}
		tokenRepository.deleteById(tokenVerification.getId());
		artisan.setEnabled(true);
		artisanDao.saveArtisan(artisan);
		ResponseStructure<String> responseStructure = new ResponseStructure<String>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("success");
		responseStructure.setData("valid");
		ResponseEntity<ResponseStructure<String>> responseEntity = new ResponseEntity<ResponseStructure<String>>(
				responseStructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Artisan>>> searchArtisan(String key) {
		List<Artisan> artisans = artisanDao.searchArtisan(key);
		if(artisans.size()>0) {
			
			ResponseStructure<List<Artisan>> responseStructure = new ResponseStructure<List<Artisan>>();
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("success");
			responseStructure.setData(artisans);
			ResponseEntity<ResponseStructure<List<Artisan>>> responseEntity = new ResponseEntity<ResponseStructure<List<Artisan>>>(
					responseStructure, HttpStatus.OK);
			return responseEntity;
		}else {
			ResponseStructure<List<Artisan>> responseStructure = new ResponseStructure<List<Artisan>>();
			responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
			responseStructure.setMessage("no result foung");
			responseStructure.setData(null);
			ResponseEntity<ResponseStructure<List<Artisan>>> responseEntity = new ResponseEntity<ResponseStructure<List<Artisan>>>(
					responseStructure, HttpStatus.NOT_FOUND);
			return responseEntity;
		}
		
	}
	
	
	

	@Override
	public Optional<Artisan> findUserByEmail(String email) {
		return Optional.ofNullable(artisanRepository.getArtisanByEmail(email));

	}

	@Override
	public void createPasswordReserTokenForUser(Artisan artisan, String token) {
		PasswordResetToken resetToken = new PasswordResetToken(token, artisan);
		passwordResetTokenRepository.save(resetToken);

	}

	@Override
	public String validatePasswordResetToken(String token) {
		PasswordResetToken passwordResetToken = passwordResetTokenRepository.findByToken(token);
		if (passwordResetToken == null) {
			return "invalid";
		}
		Calendar calendar = Calendar.getInstance();
		if ((passwordResetToken.getExpireDate().getTime() - calendar.getTime().getTime()) >= 0) {

			return "expired";

		}

		return "valid";
	}

	@Override
	public Optional<Artisan> getUserByPasswordResetToken(String token) {
		return Optional.ofNullable(passwordResetTokenRepository.findByToken(token).getArtisan());
	}

	@Override
	public void changePassword(Artisan artisan, String newPassword, String token) {
		artisan.setPassword(encoder.encode(newPassword));
		artisanRepository.save(artisan);
		passwordResetTokenRepository.delete(passwordResetTokenRepository.findByToken(token));

	}
}
