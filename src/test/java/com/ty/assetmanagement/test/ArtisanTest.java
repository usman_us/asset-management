package com.ty.assetmanagement.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ty.assetmanagement.dao.ArtisanDao;
import com.ty.assetmanagement.dto.Artisan;
import com.ty.assetmanagement.event.ResgistrationCompleteEvent;
import com.ty.assetmanagement.service.ArtisanService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArtisanTest {
	
	@Autowired
	private  ArtisanService artisanService;
	
	@MockBean
	private ArtisanDao artisanDao;
	
	private Artisan artisan = new Artisan(); 
	
	List<Artisan> artisans = new ArrayList<Artisan>();
	
	int id = 1;
	
	//@Test
	public void saveArtisanTest() {
		
		// comment publisher.publishEvent(new ResgistrationCompleteEvent(artisan, urlCreation.applicationUrl(request))); this line before testing this method in ArtisanServiceImpl
		Calendar calendar = Calendar.getInstance();
		final HttpServletRequest request = null;
		artisan.setHired_date(calendar.getTime());
		artisan.setPassword("123");
		when(artisanDao.saveArtisan(artisan)).thenReturn(artisan);
		assertEquals(artisan,artisanService.saveArtisan(artisan, 1,  request).getBody().getData());
		
	}
	
	@Test
	public void getArtisan() {
		
		when(artisanDao.getArtisanById("a")).thenReturn(artisan);
		assertEquals(artisan, artisanService.getArtisanById("a").getBody().getData());
	}
	
	@Test
	public void deleteArtisan() {
		
		when(artisanDao.deleteArtisan(artisan)).thenReturn(true);
		when(artisanDao.getArtisanById("a")).thenReturn(artisan);
		assertEquals(true, artisanService.deleteArtisan("a").getBody().getData());
	}
	
	@Test
	public void getAllArtisan() {
		
		when(artisanDao.getAllArtisans()).thenReturn(artisans);
		assertEquals(artisans, artisanService.getAllArtisan().getBody().getData());
		
	}
	
	

}
