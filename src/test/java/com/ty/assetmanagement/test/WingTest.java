package com.ty.assetmanagement.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ty.assetmanagement.dao.WingDao;
import com.ty.assetmanagement.dto.Wing;
import com.ty.assetmanagement.service.WingService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WingTest {
	
	@Autowired
	WingService wingService;

	@MockBean
	WingDao wingDao;

	@Test
	public void saveWing() {
		Wing wing = new Wing();
		when(wingDao.saveWing(wing)).thenReturn(wing);
		assertEquals(wing, wingService.saveWing(wing).getBody().getData());

	}
	

	@Test
	public void getWingById() {
		Wing wing = new Wing();
		when(wingDao.getWingById(1)).thenReturn(wing);
		assertEquals(wing, wingService.getWingById(1).getBody().getData());
	}
	
	@Test
	public void deleteWing() {
		boolean b = true;
		Wing wing = new Wing();
		when(wingDao.deleteWing(wing)).thenReturn(b);
		when(wingDao.getWingById(1)).thenReturn(wing);
		assertEquals(b, wingService.deleteWing(1).getBody().getData());
	}

	@Test
	public void updateWing() {
		Wing wing = new Wing();
		when(wingDao.updateWing(wing)).thenReturn(wing);
		when(wingDao.getWingById(1)).thenReturn(wing);
		assertEquals(wing, wingService.updateWing(wing, 1).getBody().getData());
	}



}
