package com.ty.assetmanagement.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;


import com.ty.assetmanagement.dao.impl.AssetDaoImpl;
import com.ty.assetmanagement.dto.Asset;
import com.ty.assetmanagement.service.AssetService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssetTest {
	@Autowired
	private AssetService assetService;

	@Autowired
	@MockBean
	private AssetDaoImpl assetDaoImpl;

	int id = 1;
	Asset asset = new Asset();

	@Test
	public void saveAssetTest() {

		when(assetDaoImpl.saveAsset(asset)).thenReturn(asset);
		assertEquals(asset, assetService.saveAsset(asset).getBody().getData());
	}

	@Test
	public void getAssetById() {
		when(assetDaoImpl.getAssetById(id)).thenReturn(asset);
		assertEquals(asset, assetService.getAssetById(id).getBody().getData());
	}

	@Test
	public void updateAssetTest() {
		when(assetDaoImpl.updateAsset(asset)).thenReturn(asset);
		assertEquals(asset, assetService.updateAsset(asset, id).getBody().getData());
	}

	@Test
	public void deleteAssetTest() {
		when(assetDaoImpl.deleteAsset(asset)).thenReturn(true);
		when(assetDaoImpl.getAssetById(id)).thenReturn(asset);
		assertEquals(true, assetService.deleteAsset(id).getBody().getData());
	}

	@Test
	public void getAllAssetTest() {
		ArrayList<Asset> arrayList = new ArrayList<Asset>();
		arrayList.add(asset);
		arrayList.add(asset);

	}

}
