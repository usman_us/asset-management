package com.ty.assetmanagement.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ty.assetmanagement.dao.Asset_AllocationDao;
import com.ty.assetmanagement.dto.Asset_Allocation;
import com.ty.assetmanagement.service.Asset_AllocationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Asset_AllocationTest {

	@Autowired
	private Asset_AllocationService allocationService; 
	
	@MockBean
	private Asset_AllocationDao allocationDao;
	
	@Test
	public void saveAssetAllocation() {
		Asset_Allocation  allocation = new Asset_Allocation();
		when(allocationDao.saveAsset_Allocation(allocation)).thenReturn(allocation);
		assertEquals(allocation,allocationService.saveAssetAllocation(allocation, "a",1).getBody().getData());
	}
	
	@Test
	public void getAssetAllocationById() {
		Asset_Allocation allocation = new Asset_Allocation();
		when(allocationDao.getAsset_AllocationById(1)).thenReturn(allocation);
		assertEquals(allocation, allocationService.getAssetAllocationById(1).getBody().getData());
	}
	
	@Test
	public void getllAssetAllocations() {
		List<Asset_Allocation> allocations = new ArrayList<Asset_Allocation>();
		Asset_Allocation allocation1= new Asset_Allocation();
		Asset_Allocation allocation2= new Asset_Allocation();
		Asset_Allocation allocation3= new Asset_Allocation();
		
		allocations.add(allocation1);
		allocations.add(allocation2);
		allocations.add(allocation3);
		when(allocationDao.getAllAsset_Allocation()).thenReturn(allocations);
		assertEquals(3,allocationService.getAllAllocations().getBody().getData().size());
	}
	@Test
	public void deleteAssetAllocation() {
		boolean b = true;
		Asset_Allocation allocation = new Asset_Allocation();
		when(allocationDao.deleteAsset_Allocation(allocation)).thenReturn(b);
		when(allocationDao.getAsset_AllocationById(1)).thenReturn(allocation);
		assertEquals(b,allocationService.deleteAllocation(1).getBody().getData());
	}
	@Test
	public void updateAssetAllocation() {
		Asset_Allocation allocation = new Asset_Allocation();
		when(allocationDao.updateAsset_Allocation(allocation)).thenReturn(allocation);
		when(allocationDao.getAsset_AllocationById(1)).thenReturn(allocation);
		assertEquals(allocation,allocationService.updateAllocation(allocation,1).getBody().getData());
	}
}
